#include <iostream>
using namespace std;


int main(int argc, char *argv[]){
  int i;
  cout << "You have entered " << argc << " arguments : " << '\n';
  cout << " Program Name is : ' "  << argv[0] << " ' " << '\n';
  cout << " list of arguments -- " << '\n'; 
  for( i = 1 ; i < argc ; ++i){
    cout << "'" << argv[i] <<"'"; 
  }
  return 0;
}  
